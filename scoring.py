import logging

class Rules(object):
    def __init__(self):
        self.points_per_serve = 2
        self.points_to_win = 21


class Player(object):
    def __init__(self, name = None, points = 0):
        self.id = name
        self.score = points
        self.name = name

    def alias(self):
        return self.name  # lookup id and return random name
 

class ScoreBoard(object):
    def clear(self):
        return

    def show_intro(self, p1, p2):
        return

    def show_score(self, p1, p2, server):
        return

    def show_deuce(self):
        return

    def show_advantage(self, player):
        return
        
    def show_winner(self, player):
        return

    def show_consecutive_wins(self, player, wins):
        return


class Umpire(object):
    """ Table Tennis Umpire """

    def __init__(self, scoreboard = ScoreBoard()):
        self.scoreboard = scoreboard
        self.game_in_play = False
        self.rules = Rules()
        self.scoreboard.clear()
        self.last_winner = -1
        self.consecutive_wins = 0

    def new_game(self, p1, p2):
        logging.debug("Umpire.new_game(" + p1.name + ", " + p2.name + ")")
        self.server = 1
        self.player1 = p1
        self.player2 = p2
        self.game_in_play = True
        self.rules = Rules()
        self.scoreboard.show_intro(self.player1, self.player2)
        self.last_winner = -1
        self.consecutive_wins = 0

    def step_down(self):
        self.game_in_play = False
        self.scoreboard.clear()

    def point_scored(self, player):
        logging.debug("Umpire.point_scored(" + str(player) + ")")
        if not self.game_in_play:
            return

        if player == self.last_winner:
            self.consecutive_wins += 1
            self.scoreboard.show_consecutive_wins(player, self.consecutive_wins)
        else:
            self.last_winner = player
            self.consecutive_wins = 1

        if player == 1:
            self.player1.score += 1
        else:
            self.player2.score += 1

        delta = abs(self.player1.score - self.player2.score)
        if self.player1.score > self.player2.score:
            leader = self.player1
        else:
            leader = self.player2

        if (self.player1.score + self.player2.score) % self.rules.points_per_serve == 0:
            self.server = 3 - self.server
        
        if self.player1.score >= (self.rules.points_to_win - 1) and delta == 0:
            self.scoreboard.show_deuce()
            self.rules.points_per_serve = 1
            return

        if leader.score > (self.rules.points_to_win - 1):
            if delta >= 2:
                self.game_in_play = False
                self.scoreboard.show_winner(leader)
            else:
                self.scoreboard.show_advantage(leader)
        else:
                self.scoreboard.show_score(self.player1, self.player2, self.server)
