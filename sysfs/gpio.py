import select
import logging
import os
import glob
import threading
import time

def write_once(path, value):
    f = open(path, 'w')
    f.write(value)
    f.close()


class GPIO(object):

    class Pin(object):
        def __init__(self):
            self.level_fd = None
            self.level_listeners = []

        def value(self):
            self.level_fd.seek(0)
            return str(self.level_fd.read()).strip()

        def enable(self, base_path):
            self.level_fd = open(base_path + "/value", "r")
            write_once(base_path + '/direction', 'in')
            write_once(base_path + '/edge', 'both')

        def disable(self):
            if self.level_fd != None:
                self.level_fd.close()


    def __init__(self):
        logging.debug("GPIO.__init__()")
        self.pins = {}
        self.pin_listeners = {}
        self.poller = select.epoll()
        self.listen_thread = None
        self.listen_thread_running = False


    def enable_pin(self, pin):
        logging.debug("GPIO.enable_pin(%d)" % pin)
        if pin in self.pins:
            raise IOError("pin is already enabled, disable first")
        try:
           exp = open("/sys/class/gpio/export", "w")
           exp.write(str(pin))
           exp.close()
        except: 
            pass

        time.sleep(0.1)
        pin_name = glob.glob("/sys/class/gpio/gpio" + str(pin) + "_*")
        logging.debug("GPIO.enable_pin(%d): found pin as %s" % (pin, str(pin_name)))
        if len(pin_name) == 1:
            self.pins[pin] = GPIO.Pin()
            self.pins[pin].enable(pin_name[0]) 
            self.pin_listeners[pin] = []
        else:
            raise IOError("pin name " + str(pin) + " is not unique!")


    def disable_pin(self, pin):
        logging.debug("GPIO.disable_pin(%d)" % pin)
        if pin not in self.pins:
            return

        self.pins[pin].disable()
        del self.pins[pin]
        del self.pin_listeners[pin]

        try:
            exp = open("/sys/class/gpio/unexport", "w")
            exp.write(str(pin))
            exp.close()
        except:
            pass


    def add_listener(self, pin, callback):
        logging.debug("GPIO.add_listener(%d, callback)" % (pin, ))
        if pin in self.pins:
            self.poller.register(self.pins[pin].level_fd.fileno(), select.POLLPRI)
        self.pin_listeners[pin].append(callback)


    def read_value(self, pin):
        logging.debug("GPIO.read_value(%d, %d)" % (pin, trigger))
        if pin in self.pins:
            return self.pins[pin].value()
        return None


    def polling_thread(self):
        logging.debug("GPIO.polling_thread()")
        while self.listen_thread_running:
            events = self.poller.poll(1.0)
            for fileno, event in events:
                for p in self.pins: 
                    if self.pins[p].level_fd != None and self.pins[p].level_fd.fileno() == fileno: 
                        value = self.pins[p].value()
                        for listener in self.pin_listeners[p]:
                            listener(p, value)
        logging.debug("GPIO.polling_thread() quit")


    def listen(self):
        logging.debug("GPIO.listen()")
        if not self.listen_thread_running:
            self.listen_thread = threading.Thread(target=self.polling_thread, name="Polling Thread")
            self.listen_thread_running = True
            self.listen_thread.start()


    def ignore(self):
        logging.debug("GPIO.ignore()")
        if self.listen_thread_running:
            self.listen_thread_running = False
            self.listen_thread.join(1.5)
            self.listen_thread = None

