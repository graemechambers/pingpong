import time
import unittest
import logging
from sysfs import gpio


class GPIOTest(unittest.TestCase):

    def setUp(self):
        self.listen_count = 0

    def pin_listener(self, pin, value):
        logging.debug("GPIOTest.pin_listener(" + str(pin) + ", " + str(value) + ")")
        self.listen_count += 1

    def test_gpio_basic(self):
        yeeha = gpio.GPIO()
        yeeha.listen()
        yeeha.ignore()


    def tests_listen_callback(self):
        self.listen_count = 0
        yeeha = gpio.GPIO()
        yeeha.enable_pin(61)
        yeeha.add_listener(61, self.pin_listener)
        yeeha.listen()
        time.sleep(10)
        yeeha.ignore()


if __name__ == "__main__":
    unittest.main()
