from hanover import message, sign
from sysfs import gpio
from rfid import reader
import scoring
import random
import time
import logging
import argparse
import signal
import sys
import os
from datetime import datetime, timedelta


def read_entire_file(filename):
    try:
        with open(filename) as f:
            return f.readlines()
    except:
        pass
    return []


class DTIScoreBoard(scoring.ScoreBoard):
    def __init__(self, port):
        self.sign = sign.Signage(port)
        self.clear_sounds = read_entire_file("reset.txt")
        self.intro_sounds = read_entire_file("game_intro.txt")
        self.score_sounds = read_entire_file("score.txt")
        self.deuce_sounds = read_entire_file("deuce.txt")
        self.advantage_sounds = read_entire_file("advantage.txt")
        self.winner_sounds = read_entire_file("winner.txt")

    def clear(self):
        self.sign.transmit(message.clear(0))
        if len(self.clear_sounds) > 0:
            os.system("aplay " + random.choice(self.clear_sounds))

    def show_intro(self, p1, p2):
        self.sign.transmit(message.text(p1.alias() + " versus " + p2.alias(), 0))
        if len(self.intro_sounds) > 0:
            os.system("aplay " + random.choice(self.intro_sounds))

    def show_score(self, p1, p2, server):
        if server == 1:
            text = "%02d%%* %s *<%02d%%  %s  " % ( p1.score, p1.alias(), p2.score, p2.alias())  
        else:
            text = "%02d%%  %s  <%02d%%* %s *" % ( p1.score, p1.alias(), p2.score, p2.alias())
        self.sign.transmit(message.text(text, 0))
        if len(self.score_sounds) > 0:
            os.system("aplay " + random.choice(self.score_sounds))

    def show_deuce(self):
        self.sign.transmit(message.text("deuce", 0))
        if len(self.deuce_sounds) > 0:
            os.system("aplay " + random.choice(self.deuce_sounds))

    def show_advantage(self, who):
        self.sign.transmit(message.text("Advantage " + who.alias(), 0))
        if len(self.advantage_sounds) > 0:
            os.system("aplay " + random.choice(self.advantage_sounds))

    def show_winner(self, who):
        self.sign.transmit(message.text(who.alias() + " wins", 0))
        if len(self.winner_sounds) > 0:
            os.system("aplay " + random.choice(self.winner_sounds))

    def show_consecutive_wins(self, who, wins):
        filename = "sounds/" + str(wins) + "_in_a_row.wav"
        if os.path.exists(filename):
            os.system("aplay " + filename)



class GameController(object):
    def __init__(self, umpire):
        self.umpire = umpire
        self.p1_player = 1
        self.p2_player = 2
        self.last_tagged_player = 1

    def on_player1_button(self, pin, value):
        if value == str(0):
            umpire.point_scored(self.p1_player)

    def on_player2_button(self, pin, value):
        if value == str(0):
            umpire.point_scored(self.p2_player)

    def on_reset1_button(self, pin, value):
        if value == str(0):
            one = random.choice(players_list.keys())
            umpire.new_game(scoring.Player(one), scoring.Player(players_list[one]))
            self.p1_player = 1
            self.p2_player = 2

    def on_reset2_button(self, pin, value):
        if value == str(0):
            one = random.choice(players_list.keys())
            umpire.new_game(scoring.Player(one), scoring.Player(players_list[one]))
            self.p1_player = 2
            self.p2_player = 1

    def on_player_tagged_in(self, tag, device):
        logging.debug('on_player_tagged_in(' + tag + ', ' + device + ')')



players_list = {  "Big Bad Wolf"   : "Three Little Pigs"
                , "Alien"          : "Predator" 
                , "Peter Pan"      : "Captain Hook"
                , "The Evil Queen" : "Snow White"
                , "Sabretooth"     : "Wolverine"
                , "Red Skull"      : "Captain America"
                , "Magneto"        : "Professor X"
                , "Doctor Doom"    : "Fantastic Four"
                , "The Mandarin"   : "Iron Man"
                , "Loki"           : "Thor"
                , "The Joker"      : "Batman"
                , "Lex Luthor"     : "Superman"
                , "M.Bison"        : "Chun Li"
                , "Leslie Winkle"  : "Sheldon Cooper"
                , "Doctor Claw"    : "Inspector Gadget"
                , "Dr Evil"        : "Austin Powers"
                , "Barnyard Dawg"  : "Foghorn Leghorn"
                , "Cheetah"        : "Wonder Woman"
                , "Green Goblin"   : "Spider-Man"
                , "Doctor Octopus" : "Spider-Man"
                , "Venom"          : "Spider-Man"
                , "Ming the Merciless" : "Flash Gordon"
                , "Jigsaw"         : "The Punisher"

              }

running = True


def signal_handler(signal, frame):
    logging.debug("Ctrl-C caught, quit")
    global running
    running = False


if __name__ == "__main__":    
    parser = argparse.ArgumentParser(description='Ping Pong Scoreboard')
    parser.add_argument("serialport", help="path to the scoreboard serial port uri")
    parser.add_argument("-p1pin", type=int, default=61, help="gpio pin for player 1 scoring a point")
    parser.add_argument("-p2pin", type=int, default=60, help="gpio pin for player 1 scoring a point")
    parser.add_argument("-r1pin", type=int, default=63, help="gpio pin player 1 reset button")
    parser.add_argument("-r2pin", type=int, default=62, help="gpio pin player 2 reset button")
    parser.add_argument("-rfid", type=string, default='/dev/input/event1', help='player rfid reader')
    args = parser.parse_args()
    
    logging.basicConfig(filename='/tmp/pingpong.log', level=logging.DEBUG)

    logging.debug("Connecting to scoreboard")
    umpire = scoring.Umpire(DTIScoreBoard(args.serialport))

    signal.signal(signal.SIGINT, signal_handler)

    game = GameController(umpire)
    logging.debug("Attaching GPIO")
    buttons = gpio.GPIO()

    buttons.enable_pin(args.p1pin)
    buttons.enable_pin(args.p2pin)
    buttons.enable_pin(args.r1pin)
    buttons.enable_pin(args.r2pin)
    buttons.add_listener(args.p1pin, game.on_player1_button)
    buttons.add_listener(args.p2pin, game.on_player2_button)
    buttons.add_listener(args.r1pin, game.on_reset1_button)
    buttons.add_listener(args.r2pin, game.on_reset2_button)

    logging.debug("Attaching RFID readers")
    try:
        tag_reader = RFIDReader(args.rfid)
        tag_reader.add_listener(game.on_player_tagged_in)
        tag_reader.listen()
    except:
        logging.debug('unabled to attach to RFID reader')
        tag_reader = None

    buttons.listen()

    while running:
        time.sleep(1) 

    buttons.ignore()
    if tag_reader != None:
        tag_reader.ignore()

    umpire.step_down()