import unittest
import scoring

class CountingScoreBoard(scoring.ScoreBoard):
    def __init__(self):
        self.cleared = 0
        self.intro = 0
        self.score = 0
        self.deuce = 0
        self.advantage = 0
        self.winner = 0
        self.consecutive = 0

    def clear(self):
        self.cleared += 1

    def show_intro(self, p1, p2):
        self.intro += 1

    def show_score(self, p1, p2, server):
        self.score += 1

    def show_deuce(self):
        self.deuce += 1

    def show_advantage(self, player):
        self.advantage += 1
        
    def show_winner(self, player):
        self.winner += 1

    def show_consecutive_wins(self, player, wins):
        self.consecutive += wins



class TestScoreKeeper(unittest.TestCase):
    def test_no_score(self):
        umpire = scoring.Umpire()
        umpire.new_game(scoring.Player("Player 1"), scoring.Player("Player 2"))
        self.assertEqual(umpire.player1.score, 0);
        self.assertEqual(umpire.player1.name, "Player 1");
        self.assertEqual(umpire.player2.score, 0);
        self.assertEqual(umpire.player2.name, "Player 2");

    def test_server(self):
        umpire = scoring.Umpire()
        umpire.new_game(scoring.Player("Player 1"), scoring.Player("Player 2"))
        for i in range(1, 4):  # limit to stay in game play
            self.assertEqual(umpire.server, 1);
            umpire.point_scored(1)
            self.assertEqual(umpire.server, 1);
            umpire.point_scored(1)
            self.assertEqual(umpire.server, 2);
            umpire.point_scored(1)
            self.assertEqual(umpire.server, 2);
            umpire.point_scored(1)

    def test_outright_winner(self):
        umpire = scoring.Umpire(CountingScoreBoard())
        umpire.new_game(scoring.Player("Player 1"), scoring.Player("Player 2"))
        for i in range(0, umpire.rules.points_to_win):
            umpire.point_scored(1)

        self.assertEqual(umpire.player1.score, umpire.rules.points_to_win, "player 1 scored 11 times")
        self.assertEqual(umpire.game_in_play, False, "player 1 wins")
        self.assertEqual(umpire.scoreboard.winner, 1, "winner was called")

    def test_deuce(self):
        umpire = scoring.Umpire(CountingScoreBoard())
        umpire.new_game(scoring.Player("Player 1"), scoring.Player("Player 2"))
        for i in range(0, umpire.rules.points_to_win - 1):
            umpire.point_scored(2)
            umpire.point_scored(1)
        self.assertEqual(umpire.scoreboard.deuce, 1, "deuce was called")
        even = 20
        for i in range(1, even):
            umpire.point_scored(1)
            self.assertEqual(umpire.scoreboard.advantage, i, "advantage was called")
            umpire.point_scored(2)
            self.assertEqual(umpire.scoreboard.deuce, 1+i, "deuce was called")
        umpire.point_scored(2)
        self.assertEqual(umpire.scoreboard.advantage, even, "advantage was called")
        umpire.point_scored(2)
        self.assertEqual(umpire.scoreboard.winner, 1, "winner was called")
    
    def test_consecutive(self):
        umpire = scoring.Umpire(CountingScoreBoard())
        umpire.new_game(scoring.Player("Player 1"), scoring.Player("Player 2"))
        umpire.point_scored(1)
        umpire.point_scored(1)
        self.assertEqual(umpire.scoreboard.consecutive, 2, "player had 2 in a row")
        umpire.point_scored(1)
        self.assertEqual(umpire.scoreboard.consecutive, 5, "player then had 3 in a row")
        umpire.point_scored(2)
        self.assertEqual(umpire.scoreboard.consecutive, 5, "player lost the point")
        umpire.point_scored(2)
        self.assertEqual(umpire.scoreboard.consecutive, 7, "player 2 had 2 in a row")
        umpire.point_scored(2)
        self.assertEqual(umpire.scoreboard.consecutive, 10, "player 2 had 3 in a row")

if __name__ == "__main__":
    unittest.main()
