import unittest
import time
from rfid import reader


class TestRFIDReader(unittest.TestCase):

    def tag_listener(self, tag, device):
        print "tag_listener(" + str(tag) + ", " + str(device) + ")"

    def test_clean_quit(self):
        rfid = reader.RFIDReader('/dev/input/event2')
        rfid.add_listener(self.tag_listener)
        rfid.listen()
        time.sleep(10)
        rfid.ignore()


if __name__ == "__main__":
    unittest.main()
