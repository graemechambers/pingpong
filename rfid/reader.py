from evdev import InputDevice, categorize, ecodes
import logging
import threading
import time
import select


class RFIDReader(object):

    def __init__(self, device = '/dev/input/event2'):
        logging.debug("RFIDReader.__init__(" + device + ")")
        self.dev = InputDevice(device)
        self.dev.grab()
        self.tag_listeners = []
        self.listen_thread = None
        self.listen_thread_running = False


    def add_listener(self, listener):
        logging.debug("RFIDReader.add_listener()")
        self.tag_listeners.append(listener)


    def polling_thread(self):
        logging.debug("RFIDReader.polling_thread()")
        number = []
        while self.listen_thread_running:
            r, w, e = select.select([self.dev.fileno()], [], [], 1.0)
            if len(r) > 0:
                event = self.dev.read_one()
                if event.type == ecodes.EV_KEY and event.value == 1:
                    keypress = ecodes.KEY[event.code].split('KEY_')[-1]
                    if keypress == "ENTER":
                        rfid_tag = ''.join(number)
                        number = []
                        for li in self.tag_listeners:
                            li(rfid_tag, self.dev.fn)                     
                    else:
                        number.append(keypress)

        logging.debug("RFIDReader.polling_thread() quit")


    def listen(self):
        logging.debug("RFIDReader.listen()")
        if not self.listen_thread_running:
            self.listen_thread = threading.Thread(target=self.polling_thread, name="polling thread")
            self.listen_thread_running = True
            self.listen_thread.start()


    def ignore(self):
        logging.debug("RFIDReader.ignore()")
        if self.listen_thread_running:
            self.listen_thread_running = False
            self.dev.ungrab()
            self.listen_thread.join(2.0)
            self.listen_thread = None

