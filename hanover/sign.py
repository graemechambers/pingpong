import serial, logging
from hanover import message


class Signage(object):
    """ Open a connection to Hanover signs.  
	Port can be device names: /dev/ttyUSB0 COM3
	         or urls: rfc2217://<host>:<port>[/<option>[/<option>]]
    """ 
    def __init__(self, port, baudrate=4800, timeout=5):
        logging.debug("Signage.__init__(" + port + ", " + str(baudrate) + ", " + str(timeout))
        self.port = serial.serial_for_url(port, do_not_open=True)
        self.port.baudrate = baudrate
        self.port.bytesize = serial.EIGHTBITS
        self.port.party = serial.PARITY_NONE
        self.port.stopbits = serial.STOPBITS_ONE
        self.port.timeout = timeout
        #self.port.writeTimeout = timeout
        self.port.rtscts = False
        self.port.dsrdtr = False
        self.port.open()

    def __del__(self):
        logging.debug("Signage.__del__()")
        self.port.close()
    
    def transmit(self, msg):
        logging.debug("Signage.transmit(type=" + str(msg.get_type()) + ", bytes=" + str(msg.to_bytes()) + ")")
        self.port.write(msg.to_bytes())
