import unittest
from hanover import sign, message


class TestSignage(unittest.TestCase):

    def test_loop(self):
        signage = sign.Signage("loop://")
        st = message.selftest(0)
        signage.transmit(st)

if __name__ == "__main__":
    unittest.main()
