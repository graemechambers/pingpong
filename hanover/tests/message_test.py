import unittest
from hanover import message

class TestMessageTypes(unittest.TestCase):

    def test_empty(self):
        msg = message.Message()
        self.assertEqual(0, len(msg.hex_bytes), "initially the message is empty")

    def test_text(self):
        msg_bytes = [ 0x02, 0x30, 0x31, 0x48, 0x41, 0x4E, 0x4F, 0x56, 0x45, 0x52, 0x03, 0x38, 0x39 ]
        msg = message.Message(msg_bytes)
        self.assertEqual(msg.get_type(), message.Message.Type.Text, "Message is of type Text")
        self.assertEqual(msg.get_sign(), 1, "Sign 1")
        self.assertEqual(msg.get_text(), "HANOVER", "decoded mesage says HANOVER")
        msg2 = message.text("HANOVER", 1)
        self.assertEqual(msg2.get_sign(), 1, "Sign 1")
        self.assertEqual(msg2.to_bytes(), msg.to_bytes(), "encoded message equals example message")

    def test_graphics(self):
        sprite = [ 0x0, 0x1, 0x3, 0x4, 0x1, 0x3, 0x4, 0x1, 0x3, 0x4, 0x1, 0x3, 0x4 ]
        msg = message.graphics(sprite, 2)
        self.assertEqual(msg.get_type(), message.Message.Type.Graphics, "Message is of type Graphics")
        self.assertEqual(msg.get_sign(), 2, "Sign 2")
        self.assertEqual(msg.get_graphic(), sprite, "encoded graphic is equal to provided graphic")
                
    def test_status_request(self):
        msg = message.status(3)     
        self.assertEqual(msg.get_sign(), 3, "Sign 3")
        self.assertEqual(msg.get_type(), message.Message.Type.Status, "Message is of type Status")

    def test_status_reply(self):
        msg = message.status_reply(2, message.Message.Status.MessageContentError)
        self.assertEqual(msg.get_sign(), 2, "Sign 2")
        self.assertEqual(msg.get_type(), message.Message.Type.Status, "Message is of type Status")
        self.assertEqual(msg.get_status(), message.Message.Status.MessageContentError)

    def test_selftest(self):
        msg = message.selftest(4)
        self.assertEqual(msg.get_sign(), 4, "Sign 4")
        self.assertEqual(msg.get_type(), message.Message.Type.SelfTest, "Message is of type SelfTest")

    def test_clear(self):
        msg = message.clear(0)
        self.assertEqual(msg.get_sign(), 0, "All signs")
        self.assertEqual(msg.get_type(), message.Message.Type.Clear, "Message is of type Clear")

    def test_qr_status_trace(self):
        status_bytes = [ 0x02, 0x32, 0x31, 0x03, 0x39, 0x41 ]
        status_msg = message.Message(status_bytes)
        self.assertEqual(status_msg.get_type(), message.Message.Type.Status, "Message is of type Status")
        my_status_msg = message.status(1)
        self.assertEqual(my_status_msg.to_bytes(), status_msg.to_bytes(), "status trace is same as generated status")

    def test_qr_text_trace(self):
        text_bytes = [ 0x02, 0x30, 0x30, 0x7B, 0x5C, 0x6D, 0x6F, 0x64, 0x65, 0x30, 0x20, 0x54, 0x65, 0x73, 0x74, 0x20, 0x54, 0x72, 0x61, 0x69, 0x6E, 0x7D, 0x03, 0x39, 0x36 ]
        text_msg = message.Message(text_bytes)
        self.assertEqual(text_msg.get_type(), message.Message.Type.Text, "Message is of type Text")

        my_text_msg = message.text('{\\mode0 Test Train}', 0)
        self.assertEqual(my_text_msg.to_bytes(), text_msg.to_bytes(), "text trace is same as generated text")

    def test_long_strings(self):
        text = "%02d%%  %s  <%02d%%* %s *" % ( 4, "Leslie Winkle", 2, "Sheldon Cooper")
        text_msg = message.text(text)


if __name__ == "__main__":
    unittest.main()
