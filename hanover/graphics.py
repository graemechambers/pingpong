import logging
from PIL import Image

def render_bits(pixel):
    return 0


def load(filename):
    logging.debug("graphics.load(" + filename + ")")    
    im = Image.open(filename)
    im = im.convert('1') # binary image
    return [ render_bits(p) for p in im.getdata() ]   


def export(bitmap, filename):
    logging.debug("graphics.export(" + bitmap + ", " + filename + ")")


