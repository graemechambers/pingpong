import logging
import serial

class Message(object):
    """Hanover Mesage Format"""

    class Type:
        """Message Type from the byte string"""
        Text = '0'
        Graphics = '1'
        Status= '2'
        SelfTest = '3'
        TextRightJustified = '4'
        ExtendedStatus = '9'
        Clear = 'C'

    class ControlChars:
        SOH, STX, ETX, EOT = range(1, 5)

    class Status:
        OK, MessageContentError, TransmissionError, LampFailure = range(0, 4)

    def __init__(self, bytes = []):
        self.hex_bytes = list(bytes)

    def to_bytes(self):
        return serial.to_bytes(self.hex_bytes)

    def get_type(self):
        return str(unichr(self.hex_bytes[1]))

    def get_sign(self):
        return int(chr(self.hex_bytes[2]))

    def get_text(self):
        if self.get_type() in [Message.Type.Text, Message.Type.TextRightJustified]:
            text_bytes = self.hex_bytes[3:len(self.hex_bytes)-3]
            return ''.join([chr(ch) for ch in text_bytes])
        return None

    def get_graphic(self):
        if self.get_type() == Message.Type.Graphics:
            return None
        return None

    def get_status(self):
        if self.get_type() == Message.Type.Status:
            return int(''.join([chr(ch) for ch in self.hex_bytes[3:5]]))
        return None


def checksum(bytes):
    value = 256 - (sum(bytes) % 256)
    # 'value' is converted into two ascii chars, high 4 bits, low 4 bits
    if value == 256:
        value = 0
    cs = [ ord(format(value >> 4, '01X')), ord(format(value & 0x0f, '01X')) ]
    return cs


# <stx> 0 n tt..tt <etx> ss
def text(string, sign = 0):   
    logging.debug("message.text(" + string + ", " + str(sign) + ")")
    msg = []
    msg.append(Message.ControlChars.STX)
    msg.append(ord(Message.Type.Text))
    msg.append(ord(str(sign)))
    msg += [ord(ch) for ch in string]
    msg.append(Message.ControlChars.ETX)
    msg += checksum(msg[1:])
    return Message(msg)


# <stx> 1 n ll dd..dd <etx> ss
def graphics(bitmap, sign = 0):
    logging.debug("message.graphics(" + str(bitmap) + ", " + str(sign) + ")")
    msg = []
    msg.append(Message.ControlChars.STX)
    msg.append(ord(Message.Type.Graphics))
    msg.append(ord(str(sign)))
    msg.append(len(bitmap) % 256)
    msg += bitmap
    msg.append(Message.ControlChars.ETX)
    msg += checksum(msg[1:])
    return Message(msg)


# <stx> 2 n <etx> ss
def status(sign):
    logging.debug("message.status(" + str(sign) + ")")
    msg = []
    msg.append(Message.ControlChars.STX)
    msg.append(ord(Message.Type.Status))
    msg.append(ord(str(sign)))
    msg.append(Message.ControlChars.ETX)
    msg += checksum(msg[1:])
    return Message(msg)


# <stx> 3 n <etx> ss
def selftest(sign):
    logging.debug("message.selftest(" + str(sign) + ")")
    msg = []
    msg.append(Message.ControlChars.STX)
    msg.append(ord(Message.Type.SelfTest))
    msg.append(ord(str(sign)))
    msg.append(Message.ControlChars.ETX)
    msg += checksum(msg[1:])
    return Message(msg)


# <stx> C n <etx> ss
def clear(sign):
    logging.debug("message.clear(" + str(sign) + ")")
    msg = []
    msg.append(Message.ControlChars.STX)
    msg.append(ord(Message.Type.Clear))
    msg.append(ord(str(sign)))
    msg.append(Message.ControlChars.ETX)
    msg += checksum(msg[1:])
    return Message(msg)


# <soh> 2 n dd <eot> ss 
def status_reply(sign, status):
    logging.debug("message.status_reply(" + str(sign) + ")")
    msg = []
    msg.append(Message.ControlChars.SOH)
    msg.append(ord(Message.Type.Status))
    msg.append(ord(str(sign)))
    msg.append(ord('0'))
    msg.append(ord(format(status, "01X")))
    msg.append(Message.ControlChars.EOT)
    msg += checksum(msg[1:])
    return Message(msg)
    

